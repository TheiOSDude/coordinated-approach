//
//  TeamMember.swift
//  Coordinated Approach
//
//  Created by Lee Burrows on 16/11/2018.
//  Copyright © 2018 Lee Burrows. All rights reserved.
//

import Foundation

struct TeamMember: Equatable {

    let firstName: String
    let lastName: String
    let favouriteFood: String

    init(firstName: String, lastName: String, favouriteFood: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.favouriteFood = favouriteFood
    }
}
