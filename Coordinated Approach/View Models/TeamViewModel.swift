//
//  BodiesViewModel.swift
//  Coordinated Approach
//
//  Created by Lee Burrows on 16/11/2018.
//  Copyright © 2018 Lee Burrows. All rights reserved.
//

import Foundation

struct TeamViewModel {

    var delegate: personSelectionDelegate?

    let data: [TeamMember]

    init(data: [TeamMember]) {
        self.data = data
    }

    func numberOfItems() -> Int {
        return data.count
    }

    func didSelectRow(_ row: Int) {
        delegate?.didSelectPerson(index: row)
    }

    func titleForRow(_ row: Int) -> String {
        return data[row].firstName
    }

}
