//
//  TeamDetailViewModel.swift
//  Coordinated Approach
//
//  Created by Lee Burrows on 16/11/2018.
//  Copyright © 2018 Lee Burrows. All rights reserved.
//

import Foundation

struct TeamDetailViewModel {

    private let member: TeamMember
    init(_ teamMember: TeamMember) {
        member = teamMember
    }

    var firstName: String {
        return member.firstName
    }

    var lastName: String {
        return member.lastName
    }

    var favouriteFood: String {
        return member.favouriteFood
    }


}
