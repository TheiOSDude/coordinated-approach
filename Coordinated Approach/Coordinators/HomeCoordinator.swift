//
//  AppCoordinator.swift
//  Coordinated Approach
//
//  Created by Lee Burrows on 16/11/2018.
//  Copyright © 2018 Lee Burrows. All rights reserved.
//

import UIKit
protocol personSelectionDelegate {
    func didSelectPerson(index: Int)
}


/// Hindsight, thie AppCoordinator should be like the conf video we watched. An 'App Container',
class HomeCoordinator: Coordinator {

    // MARK: - Properties
    let window: UIWindow?

    lazy var memberViewModel: TeamViewModel = {
        let ariel = TeamMember(firstName: "Ariel", lastName: "Elkin", favouriteFood: "Coconut")
        let mat = TeamMember(firstName: "Mat", lastName: "Kasiowski", favouriteFood: "Shawarma")
        let kam = TeamMember(firstName: "Kam", lastName: "Nagra", favouriteFood: "Vegtables")
        let lee = TeamMember(firstName: "Lee", lastName: "Burrows", favouriteFood: "Shawarma")

        return TeamViewModel(data: [ariel, mat, kam, lee])
    }()

    lazy var rootViewController: UINavigationController = {

        let bvc = TeamViewController()
        bvc.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        bvc.viewModel = memberViewModel
        bvc.viewModel?.delegate = self
        return UINavigationController(rootViewController: bvc)
    }()

    init(window: UIWindow?) {
        self.window = window
    }

    override func start() {
        guard let window = window else {
            return
        }

        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }

    override func finish() {

    }

}

extension HomeCoordinator: personSelectionDelegate {
    func didSelectPerson(index: Int) {
        //part of the same scene, so its my undestanding we can present the VC from here. If it was another Journey we should spin up a new coordinator, add that on and call 'start' from there? Passing in the root VC as a dependency?
        let member = memberViewModel.data[index]
        let viewModel = TeamDetailViewModel(member)
        rootViewController.pushViewController(TeamDetailViewController(viewModel: viewModel), animated: true)
    }


}
