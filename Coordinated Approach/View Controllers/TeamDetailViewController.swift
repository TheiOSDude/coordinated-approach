//
//  TeamDetailViewController.swift
//  Coordinated Approach
//
//  Created by Lee Burrows on 16/11/2018.
//  Copyright © 2018 Lee Burrows. All rights reserved.
//

import UIKit

class TeamDetailViewController: UIViewController {

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var favouriteFoodLabel: UILabel!

    var viewModel: TeamDetailViewModel?

    convenience init(viewModel: TeamDetailViewModel) {
        self.init(nibName: String.init(describing: TeamDetailViewController.self), bundle: nil)
        self.viewModel = viewModel

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        firstNameLabel.text = viewModel?.firstName
        lastNameLabel.text = viewModel?.lastName
        favouriteFoodLabel.text = viewModel?.favouriteFood
    }
}
